package ua.com.satan.rxjava.models

import com.google.gson.annotations.SerializedName

data class User(
        @SerializedName("id") val id: Int,
        @SerializedName("username") val username: String,
        @SerializedName("about") val about: String,
        @SerializedName("karma") val karma: Int,
        @SerializedName("created_at") val createdAt: String,
        @SerializedName("avg") val avg: Double,
        @SerializedName("delay") val delay: String,
        @SerializedName("submitted") val submitted: Int,
        @SerializedName("updated_at") val updatedAt: String,
        @SerializedName("submission_count") val submissionCount: Int,
        @SerializedName("comment_count") val commentCount: Int,
        @SerializedName("created_at_i") val createdAtI: Int,
        @SerializedName("objectID") val objectID: String
)