package ua.com.satan.rxjava.models

import com.google.gson.annotations.SerializedName


data class Page(
        @SerializedName("hits") val hits: MutableList<Hits>,
        @SerializedName("nbHits") val nbHits: Int,
        @SerializedName("page") val page: Int,
        @SerializedName("nbPages") val nbPages: Int,
        @SerializedName("hitsPerPage") val hitsPerPage: Int,
        @SerializedName("processingTimeMS") val processingTimeMS: Int,
        @SerializedName("exhaustiveNbHits") val exhaustiveNbHits: Boolean,
        @SerializedName("query") val query: String,
        @SerializedName("params") val params: String
)