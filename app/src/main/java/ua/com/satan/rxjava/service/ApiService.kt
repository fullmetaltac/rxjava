package ua.com.satan.rxjava.service

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import ua.com.satan.rxjava.models.Page
import ua.com.satan.rxjava.models.User


interface ApiService {
    @GET("users/{username}")
    fun user(@Path(value = "username") username: String): Single<User>

    @GET("search_by_date")
    fun page(@Query("page") page: Int, @Query("tags") tags: String = "story"): Single<Page>
}
