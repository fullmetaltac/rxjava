package ua.com.satan.rxjava.models

import com.google.gson.annotations.SerializedName


data class Url(
        @SerializedName("value") val value: String,
        @SerializedName("matchLevel") val matchLevel: String,
        @SerializedName("matchedWords") val matchedWords: List<String>
)