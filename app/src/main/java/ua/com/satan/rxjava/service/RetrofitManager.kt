package ua.com.satan.rxjava.service

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ua.com.satan.rxjava.BuildConfig


object RetrofitManager {
    val apiService: ApiService

    init {
        apiService = create()
    }

    private fun create(): ApiService {
        val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.baseUrl)
                .client(createInterceptor(HttpLoggingInterceptor.Level.BODY))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        return retrofit.create(ApiService::class.java);
    }

    private fun createInterceptor(level: HttpLoggingInterceptor.Level): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = level
        return OkHttpClient.Builder().addInterceptor(interceptor).build()
    }
}