package ua.com.satan.rxjava


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import ua.com.satan.rxjava.extensions.logCurrentThread
import ua.com.satan.rxjava.extensions.subscribeAndObserve
import ua.com.satan.rxjava.models.Page
import ua.com.satan.rxjava.service.ApiService
import ua.com.satan.rxjava.service.RetrofitManager
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {
    private val TAG: String = "rxJava@Task#"

    private val compositeDisposable by lazy { CompositeDisposable() }
    private val service: ApiService by lazy { RetrofitManager.apiService }

    //task 6.3
    private fun safeSubscribe(unit: () -> (Disposable)) {
        compositeDisposable.add(unit())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //task 1
        Single.zip(service.page(1), service.page(2), BiFunction { f: Page, s: Page -> f.hits.union(s.hits) })
                .map { it.map { it.title } }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Log.d(TAG + 1, it.toString()) }, { it.printStackTrace() })

        //task 2
        service.page(1).toObservable()
                .flatMapIterable { it.hits.subList(0, 5) }
                .flatMap { service.user(it.author).toObservable() }
                .filter { it.karma > 500 }
                .collectInto(mutableListOf<String>()) { list, item -> list.add(item.username) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Log.d(TAG + 2, it.toString()) }, { it.printStackTrace() })

        //task3
        Maybe.create<String> { if (Random().nextBoolean()) it.onSuccess("Bang!") else it.onError(IllegalArgumentException()) }
                .subscribeAndObserve()
                .subscribe({ Log.d(TAG + 3, it) }, { it.printStackTrace() })

        //task4
        Maybe.create<String> { if (Random().nextBoolean()) it.onSuccess("Bang!") else it.onComplete() }
                .subscribeAndObserve()
                .subscribe({ Log.d(TAG + 4, it) }, { it.printStackTrace() }, { Log.d(TAG + 4, "Completed without emission.") })


        //task5
        Maybe.create<String> { if (Random().nextBoolean()) it.onSuccess("Bang!") else it.onComplete() }
                .defaultIfEmpty("You're alive!")
                .toSingle()
                .subscribeAndObserve()
                .subscribe(Consumer { Log.d(TAG + 5, it) })

        //task6
        safeSubscribe {
            Single.just("6")
                    .logCurrentThread(TAG + 6)
                    .subscribeAndObserve()
                    .subscribe(Consumer { Log.d(TAG + 6, it) })
        }

        //task7
        Observable.intervalRange(0, 10, 0, 1, TimeUnit.SECONDS)
                .flatMap { service.page(it.toInt()).toObservable() }
                .buffer(2)
                .concatMap { t -> Observable.just(t.map { it.hits.map { it.author } }) }
                .subscribeAndObserve()
                .subscribe {
                    Log.d(TAG + 7, it.toString())
                }

        //task8
        val schedulers = (1..3).map { Schedulers.from(Executors.newSingleThreadExecutor()) }
        service.page(0)
                .observeOn(schedulers[0])
                .logCurrentThread(TAG + 81)
                .zipWith(service.page(1), BiFunction { f: Page, s: Page -> f.hits.union(s.hits) })
                .observeOn(schedulers[1])
                .logCurrentThread(TAG + 82)
                .map { it.map { it.author } }
                .subscribeOn(schedulers[2])
                .observeOn(schedulers[2])
                .subscribe(Consumer {
                    Log.d(TAG + 83, "Current thread: ${Thread.currentThread().name}")
                    Log.d(TAG + 83, it.toString())
                })

        //task9
        Observable.intervalRange(0, 10, 0, 1, TimeUnit.SECONDS)
                .subscribeAndObserve()
                .doOnSubscribe { Log.d(TAG + 9, "Subscribed") }
                .subscribe({
                    if (it == 7L) throw Exception("777") else Log.d(TAG + 9, it.toString())
                }, {
                    Log.d(TAG + 9, it.message)
                })

        //task10
        val subject = BehaviorSubject.create<Int>()
        subject.onNext(1)
        subject.onNext(2)
        subject.onNext(3)

        subject.subscribe { Log.d(TAG + 10, it.toString()) }

        subject.onNext(4)
        subject.onNext(5)
        subject.onNext(6)

        //task11
        service.page(0)
                .map { it.hits[3] }
                .flatMap { article -> service.user(article.author).map { UserData(it.karma, it.username, article.title) } }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(Consumer { Log.d(TAG + 11, it.toString()) })
    }

    data class UserData(
            var karma: Int,
            var name: String,
            var title: String
    )
}
