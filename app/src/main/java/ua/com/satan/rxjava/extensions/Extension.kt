package ua.com.satan.rxjava.extensions

import android.util.Log
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

//task 6.1
fun <T> Single<T>.logCurrentThread(tag: String): Single<T> {
    return this@logCurrentThread.map {
        Log.d(tag, "Current thread: ${Thread.currentThread().name}")
        it
    }
}

fun <T> Observable<T>.logCurrentThread(tag: String): Observable<T> {
    return this@logCurrentThread.map {
        Log.d(tag, "Current thread: ${Thread.currentThread().name}")
        it
    }
}

//task 6.2
fun <T> Single<T>.subscribeAndObserve(): Single<T> {
    this@subscribeAndObserve
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    return this@subscribeAndObserve
}

fun <T> Maybe<T>.subscribeAndObserve(): Maybe<T> {
    this@subscribeAndObserve
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    return this@subscribeAndObserve
}

fun <T> Observable<T>.subscribeAndObserve(): Observable<T> {
    this@subscribeAndObserve
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    return this@subscribeAndObserve
}

fun <T> Flowable<T>.subscribeAndObserve(): Flowable<T> {
    this@subscribeAndObserve
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    return this@subscribeAndObserve
}